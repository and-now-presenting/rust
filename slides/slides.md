# Rust

![Rust-lang logo](./rust-logo-blk.svg)

Note: Created by Moz. org, created by C++ devs to "replace" C++. Created to
      solve a number of issues Moz was having with C++ code.



## Why?

- Performance
- Reliability
- Productivity


### Performance

- Memory-efficient
- C++ speeds

Note: No garbage collector, no runtime. LLVM -- same target as `clang`.


### Reliability

- Type-system
- Ownership model
- Memory-safe guarantee
- Thread-safe

Note: Four types of bugs are completely eliminated just by using Rust.
      Rust types are both simple enough to use for all development, but offer
      such a breadth of features as to create very sophisticated systems and
      modules.
      Rust's ownership model removes the question of who can access any given
      address.
      Rust Compiler manages memory so that humans don't have to.
      Parallelism is baked into Rust. tokio. Rayon.


### Productivity

- Documentation
- Useful compiler
- Tooling
- Simple, readable syntax
- Largely paradigm agnostic

Note: docs.rs, built in documentation (like Scala)
      Compiler warnings and errors parse code to give developers useful and
      relevant messages and hints.
      Built-in autoformatter and auto-complete (via LS), cargo package manager.
      Familiar syntax balancing need for succinct code and expressive code.
      FP, imperative. OOP



## What Can Rust Do?

- Kernel Modules
- System services
- Web apps
- User applications
- Embedded development
- "Scripting"

Note: Linux Foundation kernel adoption. Google's Fuscia OS.
      AWS' custom container runtime. Cloudflare.
      WASM native target. Wasmer project.
      Discord.
      MS' Azure IoT.
      Ergonomics of Rust make for quick development with little boilerplate



```rust[1-43|1-2|4-5|7-21|9-20|10-12|10|11|12-19|23-32|34-43|34|35-40|35|36|37|38|39|40|41|42]
use std::env;
use std::error::Error;

// Note, even using `i128` we can only calculate the 183rd number of the Fib.
// seq., ie `78,569,350,599,398,894,027,251,472,817,058,687,522`

fn fib(n: i128) -> i128 {
    let negative = n.is_negative();
    match n {
        0 => 0,
        1..=2 => 1,
        _ => {
            let (f, _) = _fib(n);
            if negative && n % 2 == 0 {
                return f * -1
            } else {
                return f
            }
        }
    }
}

fn _fib(n: i128) -> (i128, i128) {
    if n == 0 { return (0, 1) };
    let (a, b) = _fib(n / 2);
    let c = a * (b * 2 - a);
    let d = a * a + b * b;
    match n % 2 {
        0 => (c, d),
        _ => (d, c+d),
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let nth: i128 = env::args()
        .collect::<Vec<String>>()
        .iter()
        .nth(1)
        .unwrap()
        .parse::<i128>()?;
    println!("{}", fib(nth));
    Ok(())
}
```



## Fin
