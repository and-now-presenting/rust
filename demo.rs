use std::env;
use std::error::Error;

// Note, even using `i128` we can only calculate the 183rd number of the Fib.
// seq., ie `78,569,350,599,398,894,027,251,472,817,058,687,522`

fn fib(n: i128) -> i128 {
    let negative = n.is_negative();
    match n {
        0 => 0,
        1..=2 => 1,
        _ => {
            let (f, _) = _fib(n);
            if negative && n % 2 == 0 {
                return f * -1
            } else {
                return f
            }
        }
    }

}

fn _fib(n: i128) -> (i128, i128) {
    if n == 0 { return (0, 1) };
    let (a, b) = _fib(n / 2);
    let c = a * (b * 2 - a);
    let d = a * a + b * b;
    match n % 2 {
        0 => (c, d),
        _ => (d, c+d),
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let nth: i128 = env::args()
        .collect::<Vec<String>>()
        .iter()
        .nth(1)
        .unwrap()
        .parse::<i128>()?;
    println!("{}", fib(nth));
    Ok(())
}
